import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { UserSearchPage } from '../pages/user-search/user-search';
import { SearchHistoryPage } from '../pages/search-history/search-history';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { ProfilePage } from '../pages/profile/profile';
import { HttpClientModule } from '@angular/common/http'
import 'rxjs/add/operator/map'


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/user/user';
import { UserDetailsProvider } from '../providers/user-details/user-details';

@NgModule({
  declarations: [
    MyApp,
    UserSearchPage,
    SearchHistoryPage,
    TabsControllerPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    UserSearchPage,
    SearchHistoryPage,
    TabsControllerPage,
    ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    UserDetailsProvider
  ]
})
export class AppModule {


}