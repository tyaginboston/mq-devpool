import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-search-history',
  templateUrl: 'search-history.html'
})
export class SearchHistoryPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page

  items: any = [
    { "searchTerm" : "John",  "isFav": false },
    { "searchTerm" : "Matt", "isFav": true  },
    { "searchTerm" : "Cindy", "isFav": false },
    { "searchTerm" : "Carl",  "isFav": true },
    { "searchTerm" : "Bob", "isFav": false },
    { "searchTerm" : "cathy",  "isFav": false },
    { "searchTerm" : "mike",  "isFav": false }
  ]
  constructor(public navCtrl: NavController) {
  }
}
