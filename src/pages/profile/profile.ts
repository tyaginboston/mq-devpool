import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map'

import { UserDetailsProvider } from '../../providers/user-details/user-details'

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})

export class ProfilePage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  userInfo: any;
  detailItem: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private userDetailProvider: UserDetailsProvider) {
   this.userInfo = this.navParams.get('userInfo') ;
   
   this.userDetailProvider.getUserDetail(this.userInfo.login)
      .subscribe((data:any []) =>  {
        this.detailItem = data;
      }
    )
  }
}
