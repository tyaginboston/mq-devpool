import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { UserProvider } from '../../providers/user/user'
import 'rxjs/add/operator/map'

@Component({
  selector: 'page-user-search',
  templateUrl: 'user-search.html'
})
export class UserSearchPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page

  searchQuery: string = '';
  itemList = [];
  results;
  searchTerm: string;

  constructor(public navCtrl: NavController, private userProvider: UserProvider) {}

  initializeItems(val) {

    this.userProvider.getUser(val)
      .subscribe((data:any []) =>  {
        this.itemList = data;
      }
    )
  }
  getItems(ev: any) {
   
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    
    if (val && val.trim() != '') {
      this.initializeItems(val);
      // this.itemList = this.itemList.filter((item) => {
      //   return (item.toString().toLowerCase().indexOf(val.toString().toLowerCase()) > -1);
      // })
    }
  }
 
  goToProfile(params) {
   // if (!params) params = {};
    console.log(params)
    this.navCtrl.push(ProfilePage, {userInfo:params});
    
  }
}
