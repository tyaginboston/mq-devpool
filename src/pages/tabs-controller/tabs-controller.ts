import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserSearchPage } from '../user-search/user-search';
import { SearchHistoryPage } from '../search-history/search-history';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = UserSearchPage;
  tab2Root: any = SearchHistoryPage;
  constructor(public navCtrl: NavController) {
  }
  
}
