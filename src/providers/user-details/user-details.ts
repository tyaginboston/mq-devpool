import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserDetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const API: string = "https://api.github.com/users/";

@Injectable()

export class UserDetailsProvider {

  constructor(public http: HttpClient) {}

  getUserDetail(userDetails: string ): Observable<any> {
    return this.http.get(API+ userDetails )
        .map((response:any []) => response);
  }

}
