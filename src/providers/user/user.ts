import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';


const API: string = "https://api.github.com/search/users?q=";

@Injectable()

export class UserProvider {

  constructor(public http: HttpClient) {}
/*   public defaultUser: string = 'tyaginboston';
  getUser(user: string = this.defaultUser ): Observable<any> {
    return this.http.get(API+ user + '/repos')
 */
   getUser(user: string ): Observable<any> {
    //getUser(user: string ): Observable<any> {
    return this.http.get(API+ user )
        .map((response:any []) => response)
      
  }

}


