# Git User List using Angular 5/ Ionic 3

This app used Angular 5, Ionic 3 to do Git User Srach and show user profile information. All the data is retrieved from GIT Rest API.

## Getting Started

* [Download the installer](https://nodejs.org/) for Node.js 6 or greater.
* Install the ionic CLI globally: `npm install -g ionic`
* git clone https://tyaginboston@bitbucket.org/tyaginboston/mq-devpool.git
* cd mq-devpool
* ionic start . --no-overwriting

* ? Looks like a fresh checkout! No ./node_modules directory found. Would you like to install pr
oject dependencies? (Y/n): y

* ? You are already in an Ionic project directory. Do you really want to start another project h
ere? (y/N): n

* ionic cordova emulate ios --target "iPhone-7-Plus, 11.3" --livereload


